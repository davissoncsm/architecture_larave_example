<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;

class CustomExceptionHandler extends Exception
{
    private $ex;
    private $customMessage;

    public function __construct($ex, $customMessage, $message, $code, Exception $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
        $this->ex = $ex;
        $this->customMessage = $customMessage;
    }

    public function render()
    {
        return response()->json($this->customMessage,500);
    }

    public function report()
    {
        switch (true){
            case $this->ex instanceof QueryException:
                Log::critical(
                    "\n" .
                    "Exception: " . $this->getMessage() . "\n" .
                    'Line: ' . $this->getLine() . "\n" .
                    'File: ' . $this->getFile() . "\n"
                );
                break;
            default:
                Log::emergency(
                    "\n" .
                    "Exception: " . $this->getMessage() . "\n" .
                    'Line: ' . $this->getLine() . "\n" .
                    'File: ' . $this->getFile() . "\n"
                );
                break;
        }
    }
}
