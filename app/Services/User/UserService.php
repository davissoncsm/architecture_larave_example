<?php
declare(strict_types=1);

namespace App\Services\User;

use App\Repository\Contracts\IUserRepository;
use Illuminate\Support\Arr;

class UserService
{
    /**
     * @var IUserRepository
     */
    private $repository;

    /**
     * UserService constructor.
     * @param IUserRepository $repository
     */
    public function __construct(IUserRepository $repository)
    {
       $this->repository = $repository;
    }

    /**
     * @return object
     */
    public function fetchAll()
    {
        return $this->repository->getAll();
    }

    /**
     * @param int $id
     * @return object
     */
    public function fetchById(int $id)
    {
        return $this->repository->getById($id);
    }

    /**
     * @param array $data
     * @return object
     * @throws \App\Exceptions\CustomExceptionHandler
     */
    public function register(array $data)
    {
        $password = Arr::get($data, 'password');
        Arr::set($data, 'password', bcrypt($password));

        return $this->repository->store($data);
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     * @throws \App\Exceptions\CustomExceptionHandler
     */
    public function update(int $id, array $data)
    {
        return $this->repository->update($id, $data);
    }

    /**
     * @param int $id
     * @return mixed
     * @throws \App\Exceptions\CustomExceptionHandler
     */
    public function delete(int $id)
    {
        return $this->repository->delete($id);
    }
}
