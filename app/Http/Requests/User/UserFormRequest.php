<?php

namespace App\Http\Requests\User;

use App\Rules\CpfValidatorRule;
use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $segment = $this->segment(3);

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
                break;
            case 'POST':
                return [
                    'name' => 'required|string',
                    'cpf' => [
                        'required',
                        'digits:11',
                        'unique:pgsql.users,cpf',
                        new CpfValidatorRule()
                    ],
                    'email' => 'required|email|unique:pgsql.users,email',
                    'password' => 'required',
                ];
                break;
            case 'PUT':
                return [
                    'name' => 'required|string',
                    'cpf' => [
                        'required',
                        'digits:11',
                        'unique:pgsql.users,cpf,' . $segment,
                        new CpfValidatorRule()
                    ],
                    'email' => 'required|email|unique:pgsql.users,email,' . $segment,
                    'password' => 'required',
                ];
                break;
        }

    }

    public function messages()
    {
        return [
            'name.required' => 'O campo nome é obrigatório',
            'name.string' => 'O campo nome deve conter apenas letras',

            'cpf.required' => 'O campo CPF é obrigatório',
            'cpf.digits' => 'O campo CPF deve conter 11 digitos',
            'cpf.unique' => 'O CPF informado já está em uso',

            'email.required' => 'O campo email é obrigatório',
            'email.email' => 'O campo email esta em um formato inválido',
            'email.unique' => 'O campo email ja está em uso',

            'password.required' => 'O campo password é obrigatório',
        ];
    }
}
