<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Standard\StandardController;
use App\Http\Requests\User\UserStandardFormRequest;
use App\Services\User\UserService;

class UserStandardController extends StandardController
{
    public function __construct(UserService $service)
    {
        parent::__construct($service);

        $this->request = app(UserStandardFormRequest::class);
    }

}
