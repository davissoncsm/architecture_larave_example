<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserFormRequest;
use App\Http\Resources\User\UserResource;
use App\Services\User\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $users =  $this->service->fetchAll();

        return UserResource::collection($users);
    }

    /**
     * @param int $id
     * @return UserResource
     */
    public function show(int $id)
    {
        $user =  $this->service->fetchById($id);

        return new UserResource($user);
    }

    /**
     * @param UserFormRequest $request
     * @return UserResource
     * @throws \App\Exceptions\CustomExceptionHandler
     */
    public function store(UserFormRequest $request)
    {
        $store =  $this->service->register($request->all());

        return new UserResource($store);
    }

    /**
     * @param int $id
     * @param UserFormRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CustomExceptionHandler
     */
    public function update(int $id, UserFormRequest $request)
    {
        $update = $this->service->update($id, $request->all());

        if($update){
            return response()->json(['message' => 'Atualizado com sucesso'], 204);
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CustomExceptionHandler
     */
    public function delete(int $id)
    {
        $delete =  $this->service->delete($id);

        if($delete){
            return response()->json(['message' => 'Removido com sucesso'], 204);
        }
    }
}
