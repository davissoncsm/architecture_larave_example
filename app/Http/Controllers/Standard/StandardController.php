<?php

namespace App\Http\Controllers\Standard;

use App\Http\Controllers\Controller;

class StandardController extends Controller
{
    /**
     * @var $service service layer
     */
    protected $service;
    /**
     * @var $request request layer
     */
    protected $request;

    /**
     * StandardController constructor.
     * @param $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->service->fetchAll();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->service->fetchById($id);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        $create =  $this->service->register($this->request->all());

        if($create){
            return response()->json(['message' => 'created successfully'], 201);
        }
    }

    /**
     * @param int $id entity identify
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(int $id)
    {
        $update =  $this->service->update($id, $this->request->all());

        if($update){
            return response()->json([], 204);
        }
    }

    /**
     * @param int $id entity identify
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id)
    {
        $delete =  $this->service->delete($id);

        if($delete){
            return response()->json([], 204);
        }
    }
}
