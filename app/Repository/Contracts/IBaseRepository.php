<?php

declare(strict_types=1);

namespace App\Repository\Contracts;

interface IBaseRepository
{
    /**
     * Return all data.
     */
    public function getAll(): object;

    /**
     * Return data by id
     *
     * @param int $id
     * @return object
     */
    public function getById(int $id): object;

    /**
     * Retrieve query by columns
     *
     * @param array $columns
     * @return object
     */
    public function getByColumns(array $columns = []): object;

    /**
     * @param array $data : Data to be recorded in the database
     */
    public function store(array $data): object;

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data);

    /**
     * @param int $id
     * @return mixed
     */
    public function delete(int $id);


}
