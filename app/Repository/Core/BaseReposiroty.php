<?php

declare(strict_types=1);

namespace App\Repository\Core;

use App\Exceptions\CustomExceptionHandler;
use App\Repository\Contracts\IBaseRepository;
use App\Repository\Exception\NotEntityDefined;
use Illuminate\Support\Facades\DB;

class BaseReposiroty implements IBaseRepository
{
    protected $entity;

    public function __construct()
    {
        $this->entity = $this->resolvEntity();
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|mixed
     * @throws NotEntityDefined
     *
     */
    public function resolvEntity()
    {
        if (!method_exists($this, 'entity')) {
            throw new NotEntityDefined();
        }

        return app($this->entity());
    }

    /**
     * Retrieve all entity data.
     */
    public function getAll(): object
    {
        return $this->entity->get();
    }

    /**
     * @param int $id
     * @return object
     */
    public function getById(int $id): object
    {
        return $this->entity->findOrFail($id);
    }

    /**
     * Retrieve query by columns
     *
     * @param array $columns
     * @return object
     */
    public function getByColumns(array $columns = []): object
    {
        if(empty($columns)){
            return $this->entity->select('*')->get();
        }

        return  $this->entity->select($columns)->get();
    }


    /**
     * Wites a new record to the database.
     *
     * @param array $data : Data to be recorded in the database
     */
    public function store(array $data): object
    {
        try {
            DB::beginTransaction();

            $store = $this->entity->create($data);

            DB::commit();

            return $store->refresh();

        } catch (\Exception $e) {
            DB::rollBack();

            throw new CustomExceptionHandler(
                $e,
                'Não foi possível concluir essa operação',
                $e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

    /**
     * update record in database by ID.
     *
     * @param int   $id   :  Entities id code
     * @param array $data : Data to be recorded in the database
     */
    public function update(int $id, array $data)
    {
        try {
            DB::beginTransaction();

            $update = $this->entity->findOrfail($id)->update($data);

            DB::commit();

            return $update;

        } catch (\Exception $e) {
            DB::rollBack();

            throw new CustomExceptionHandler(
                $e,
                'Não foi possível concluir essa operação',
                $e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

    /**
     * Remove record in database by ID.
     *
     * @param int $id : Entities id code
     *
     * @return mixed
     */
    public function delete(int $id)
    {
        try {
            DB::beginTransaction();

             $delete = $this->entity->findOrfail($id)->delete();

            DB::commit();

            return $delete;

        } catch (\Exception $e) {
            DB::rollBack();

            throw new CustomExceptionHandler(
                $e,
                'Não foi possível concluir essa operação',
                $e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }
    }

}
