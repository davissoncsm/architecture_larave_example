<?php

declare(strict_types=1);

namespace App\Repository\Core;

use App\Entities\User;
use App\Repository\Contracts\IUserRepository;

class UserReposiroty extends BaseReposiroty implements IUserRepository
{
    /**
     * Make object Client ORM.
     *
     * @return void
     */
    public function entity()
    {
        return User::class;
    }

}
