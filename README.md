
## Sobre o projeto

O objetivo desse projeto é simplesmente servir de modelo, e apresentar uma dentre inumeras formas de arquitetar uma api usando o Laravel. 
Abaixo algumas caracteristicas.

- Alterado a camada de *Models* para *Entities*
- Adicionado uma camada de serviços
- Adicionado uma camada de *Repositories*
- Adicionado uma camada de *Request*
- Adicionado uma camada de *Regras personalizadas*
- Adicionado uma camada de tratamento do *Response*
- Criado uma classe de *Exceptions* customizadas