<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\UserStandardController;

Route::group(['prefix' => 'user'], function(){
    Route::get('/',[UserController::class, 'index']);
    Route::get('/{id}',[UserController::class, 'show']);
    Route::post('/create',[UserController::class, 'store']);
    Route::put('/{id}',[UserController::class, 'update']);
    Route::delete('/{id}',[UserController::class, 'delete']);
});

Route::group(['prefix' => 'standard/user'], function(){
    Route::get('/',[UserStandardController::class, 'index']);
    Route::get('/{id}',[UserStandardController::class, 'show']);
    Route::post('/create',[UserStandardController::class, 'store']);
    Route::put('/{id}',[UserStandardController::class, 'update']);
    Route::delete('/{id}',[UserStandardController::class, 'delete']);
});
